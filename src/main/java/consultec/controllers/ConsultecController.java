package consultec.controllers;

import consultec.business.ClientBusiness;
import consultec.business.ConcesionariosBusiness;
import consultec.business.dto.NewClientRequestDTO;
import consultec.business.exceptions.ClientNotFoundException;
import consultec.dao.models.ClientModel;
import consultec.dao.models.ConcessionaireModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ConsultecController {
  private static final Logger logger = LogManager.getLogger(ConsultecController.class);

  @Autowired
  ClientBusiness clientBusiness;

  @Autowired
  ConcesionariosBusiness concesionariosBusiness;

  @PostMapping(value = "/consultec/api/client", produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=" + MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity saveClient(@RequestBody NewClientRequestDTO newClientRequestDTO) {
    logger.info("Atendiendo peticion de guradado de cliente");
    clientBusiness.save(newClientRequestDTO);
    return new ResponseEntity(HttpStatus.CREATED);
  }

  @PatchMapping(value = "/consultec/api/client", produces = MediaType.APPLICATION_JSON_VALUE, headers = "Accept=" + MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<String> updateClient(@RequestBody ClientModel clientModel) {
    logger.info("Atendiendo peticion de actualizacion de cliente");
    try {
      clientBusiness.update(clientModel);
      return new ResponseEntity<String>(HttpStatus.OK);
    } catch (ClientNotFoundException e) {
      return new ResponseEntity<String>(e.getMessage(), HttpStatus.NOT_FOUND);
    }
  }

  @DeleteMapping(value = "/consultec/api/client/{clientKey}")
  public ResponseEntity<String> deleteClient(@PathVariable String clientKey) {
    logger.info("Atendiendo peticion de borrado de cliente");
    try {
      clientBusiness.deleteClientByClientKey(clientKey);
      return new ResponseEntity<String>(HttpStatus.OK);
    } catch (ClientNotFoundException e) {
      return new ResponseEntity<String>(e.getMessage(), HttpStatus.NOT_FOUND);
    }
  }

  @GetMapping(value = "/consultec/api/client/{clientKey}")
  public ResponseEntity<ClientModel> getClient(@PathVariable String clientKey) {
    logger.info("Atendiendo peticion de busqueda de cliente por id");
    ClientModel clientModel = clientBusiness.getClientByKey(clientKey);
    return new ResponseEntity<ClientModel>(clientModel, HttpStatus.OK);
  }

  @GetMapping(value = "/consultec/api/concessionaire/clients/{concessionaireKey}")
  public ResponseEntity<List<ClientModel>> getClientsByConcessionaire(@PathVariable String concessionaireKey) {
    logger.info("Atendiendo peticion de obtener clientes por concesionaria");
    List<ClientModel> clientModelList = clientBusiness.getAllClientsByConcessionaire(concessionaireKey);
    return new ResponseEntity<List<ClientModel>>(clientModelList, HttpStatus.OK);
  }

  @GetMapping(value = "/consultec/api/concessionaires")
  public ResponseEntity<List<ConcessionaireModel>> getConcessionaire() {
    logger.info("Atendiendo peticion de obtener concesionarias");
    List<ConcessionaireModel> concessionaireModelList = concesionariosBusiness.getConcesionarios();
    return new ResponseEntity<List<ConcessionaireModel>>(concessionaireModelList, HttpStatus.OK);
  }

  @GetMapping(value = "/consultec/api/clients",
    consumes = MediaType.APPLICATION_JSON_VALUE,
    produces = MediaType.APPLICATION_JSON_VALUE,
    headers = "Accept=" + MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<List<ClientModel>> getClients() {
    logger.info("Atendiendo peticion de obtener todos los clientes");
    List<ClientModel> clientModelList = clientBusiness.getAllClients();
    return new ResponseEntity<List<ClientModel>>(clientModelList, HttpStatus.OK);
  }

  @GetMapping(value = "/consultec/api/status")
  public ResponseEntity<String> getStatus() {
    logger.info("Asp is alive");
    return new ResponseEntity<String>("Api SD is Alive", HttpStatus.OK);
  }

  @GetMapping(value = "/consultec/private/status")
  public ResponseEntity<String> getStatusPrivate() {
    logger.info("Asp is alive");
    return new ResponseEntity<String>("Api SD is Alive", HttpStatus.OK);
  }
}
