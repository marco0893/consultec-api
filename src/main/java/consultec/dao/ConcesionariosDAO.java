package consultec.dao;

import consultec.dao.models.ConcessionaireModel;

import java.util.List;

public interface ConcesionariosDAO {

  List<ConcessionaireModel> getConcesionarios();
}
