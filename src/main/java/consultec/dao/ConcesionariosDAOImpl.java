package consultec.dao;

import consultec.dao.models.ConcessionaireModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
class ConcesionariosDAOImpl implements ConcesionariosDAO {

  @Autowired
  ConcesionariosRepository concesionariosRepository;

  @Override
  public List<ConcessionaireModel> getConcesionarios() {
    return concesionariosRepository.findAll();
  }
}
