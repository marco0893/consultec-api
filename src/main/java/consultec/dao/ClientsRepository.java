package consultec.dao;

import consultec.dao.models.ClientModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface ClientsRepository extends JpaRepository<ClientModel, String> {

  List<ClientModel> findAllByConcessionaireKeyAndActiveIsTrue(String concessionaireKey);

  List<ClientModel> findAllByActiveTrue();

  ClientModel findByClientKeyAndActiveIsTrue(String clientKey);

  @Modifying
  @Transactional
  @Query(value = "UPDATE clientes SET active = false WHERE client_key=?1", nativeQuery = true)
  void deleteByClientKey(String clientKey);
}
