package consultec.dao.models;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Data
@Entity
@Table(name = "clientes")
public class ClientModel {

  @Id
  @Column(name = "client_key")
  private String clientKey;

  @Column(name = "nombre")
  private String name;

  @Column(name = "apellido")
  private String lastName;

  @Column(name = "edad")
  private int age;

  @Column(name = "last_update")
  private Date lastUpdate;

  @Column(name = "puesto")
  private String position;

  @Column(name = "concesion_key")
  private String concessionaireKey;

  @Column(name = "active")
  private boolean active;
}
