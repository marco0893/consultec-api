package consultec.dao.models;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Data
@Entity
@Table(name = "concesionarias")
public class ConcessionaireModel {
  @Id
  @Column(name = "concesion_key")
  private String concesionKey;

  @Column(name = "nombre")
  private String name;

  @Column(name = "direccion")
  private String direccion;

  @Column(name = "last_update")
  private Date lastUpdate;

}
