package consultec.dao;

import consultec.dao.models.ConcessionaireModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ConcesionariosRepository extends JpaRepository<ConcessionaireModel, String> {
}
