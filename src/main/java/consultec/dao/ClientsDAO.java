package consultec.dao;

import consultec.dao.models.ClientModel;

import java.util.List;

public interface ClientsDAO {

  public List<ClientModel> getAllClientsByConcessionaire(String concessionaireKey);

  public List<ClientModel> getAllClients();

  public ClientModel getClientByKey(String clientKey);

  public void saveOrUpdateClient(ClientModel clientModel);

  public void deleteClientByClientKey(String clientKey);

}
