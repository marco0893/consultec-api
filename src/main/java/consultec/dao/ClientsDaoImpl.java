package consultec.dao;

import consultec.dao.models.ClientModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClientsDaoImpl implements ClientsDAO {
  private static final Logger logger = LogManager.getLogger(ClientsDaoImpl.class);
  @Autowired
  ClientsRepository clientsRepository;

  @Override
  public List<ClientModel> getAllClientsByConcessionaire(final String concessionaireKey) {

    return clientsRepository.findAllByConcessionaireKeyAndActiveIsTrue(concessionaireKey);
  }

  @Override
  public List<ClientModel> getAllClients() {
    return clientsRepository.findAllByActiveTrue();
  }

  @Override
  public ClientModel getClientByKey(final String clientKey) {
    return clientsRepository.findByClientKeyAndActiveIsTrue(clientKey);
  }

  @Override
  public void saveOrUpdateClient(final ClientModel clientModel) {
    clientsRepository.save(clientModel);
  }

  @Override
  public void deleteClientByClientKey(final String clientKey) {
    clientsRepository.deleteByClientKey(clientKey);
  }
}
