package consultec.business;

import consultec.dao.models.ConcessionaireModel;

import java.util.List;

public interface ConcesionariosBusiness {

  List<ConcessionaireModel> getConcesionarios();
}
