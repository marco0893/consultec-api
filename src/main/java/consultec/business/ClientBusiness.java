package consultec.business;

import consultec.business.dto.NewClientRequestDTO;
import consultec.dao.models.ClientModel;

import java.util.List;

public interface ClientBusiness {

  public List<ClientModel> getAllClientsByConcessionaire(String concessionaireKey);

  public ClientModel getClientByKey(String clientKey);

  public void save(NewClientRequestDTO newClientRequestDTO);

  public void update(ClientModel clientModel);

  public void deleteClientByClientKey(String clientKey);

  public List<ClientModel> getAllClients();
}
