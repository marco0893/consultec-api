package consultec.business;

import consultec.business.dto.NewClientRequestDTO;
import consultec.business.exceptions.ClientNotFoundException;
import consultec.dao.ClientsDAO;
import consultec.dao.models.ClientModel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class ClientBusinessImpl implements ClientBusiness {
  private static final Logger logger = LogManager.getLogger(ClientBusinessImpl.class);
  private ClientsDAO clientsDAO;

  @Autowired
  public ClientBusinessImpl(ClientsDAO clientsDAO) {
    this.clientsDAO = clientsDAO;
  }

  @Override
  public List<ClientModel> getAllClientsByConcessionaire(final String concessionaireKey) {
    logger.info("ClientBusinessImpl: getAllClientsByConcessionaire, Obteniendo lista de clientes por consecion");
    return clientsDAO.getAllClientsByConcessionaire(concessionaireKey);
  }

  @Override
  public ClientModel getClientByKey(final String clientKey) {
    logger.info("ClientBusinessImpl: getClientByKey, buscando a cliente: {}", clientKey);
    return clientsDAO.getClientByKey(clientKey);
  }

  @Override
  public void save(final NewClientRequestDTO newClientRequestDTO) {
    ClientModel clientModel = buildClientModel(newClientRequestDTO);
    logger.info("Guardando cliente: {}", clientModel);
    clientsDAO.saveOrUpdateClient(clientModel);
  }

  @Override
  public void update(final ClientModel clientModel) {
    logger.info("Actualizando informacion de cliente: {}", clientModel);
    if (clientsDAO.getClientByKey(clientModel.getClientKey()) != null) {
      clientsDAO.saveOrUpdateClient(clientModel);
    } else {
      logger.info("Cliente no encontrado: {}", clientModel);
      throw new ClientNotFoundException("No se encuentra cliente en el sistema");
    }
  }

  @Override
  public void deleteClientByClientKey(final String clientKey) {
    logger.info("Eliminando cliente: {}", clientKey);
    if (clientsDAO.getClientByKey(clientKey) != null) {
      clientsDAO.deleteClientByClientKey(clientKey);
    } else {
      logger.info("No se encuentra el usuario: {}", clientKey);
      throw new ClientNotFoundException("No se encuentra cliente en el sistema");
    }
  }

  @Override
  public List<ClientModel> getAllClients() {
    logger.info("Obteniendo lista de clientes");
    return clientsDAO.getAllClients();
  }

  private ClientModel buildClientModel(NewClientRequestDTO newClientRequestDTO) {
    String key = UUID.randomUUID().toString();
    ClientModel clientModel = new ClientModel();
    BeanUtils.copyProperties(newClientRequestDTO, clientModel);
    clientModel.setClientKey(key);
    return clientModel;
  }
}
