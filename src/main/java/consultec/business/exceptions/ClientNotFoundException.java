package consultec.business.exceptions;

public class ClientNotFoundException extends RuntimeException {
  public ClientNotFoundException(String msg) {
    super(msg);
  }

  public ClientNotFoundException() {
  }
}
