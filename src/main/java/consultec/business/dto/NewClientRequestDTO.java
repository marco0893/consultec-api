package consultec.business.dto;

import lombok.Data;

import javax.persistence.Column;
import java.util.Date;

@Data
public class NewClientRequestDTO {

  private String name;

  private String lastName;

  private int age;

  private String position;

  private String concessionaireKey;

  private boolean active;

}
