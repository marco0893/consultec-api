package consultec.business;

import consultec.dao.ConcesionariosDAO;
import consultec.dao.models.ConcessionaireModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
class ConcesionariosBusinessImpl implements ConcesionariosBusiness {

  private ConcesionariosDAO concesionariosDAO;

  @Autowired
  public ConcesionariosBusinessImpl(ConcesionariosDAO concesionariosDAO) {
    this.concesionariosDAO = concesionariosDAO;
  }

  @Override
  public List<ConcessionaireModel> getConcesionarios() {
    return concesionariosDAO.getConcesionarios();
  }
}
